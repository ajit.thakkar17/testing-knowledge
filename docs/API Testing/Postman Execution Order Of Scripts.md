**Execution order of scripts**

In Postman, the script execution order for a single request looks like this:

- A pre-request script associated with a request will execute before the request is sent
- A test script associated with a request will execute after the request is sent

![Workflow for single request](1.jpeg)

For every request in a collection, scripts will execute in the following order:

- A pre-request script associated with a collection will run prior to every request in the collection.
- A pre-request script associated with a folder will run prior to every direct child request in the folder.
- A test script associated with a collection will run after every request in the collection.
- A test script associated with a folder will run after every direct child request in the folder.

![workflow for request in collection](2.jpeg)

For every request in a collection, the scripts will always run according to the same hierarchy. Collection-level scripts (if any) will run first, then folder-level scripts (if any), and then request-level scripts (if any). Note that this order of execution applies to both pre-request and test scripts.