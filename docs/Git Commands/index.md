# GIT

GIT, which stands for Global Information Tracker, is a powerful and widely-used version control system commonly used for software development and other collaborative projects.

GIT allows multiple developers/testers to work on a project simultaneously while ensuring that their changes do not interfere with one another.

It keeps track of all the changes made to the project and allows to revert to previous versions if necessary.

You can find the complete Git guide via this link. [Git - Reference (git-scm.com)](https://git-scm.com/docs)

### Git Diagrams

![](git.png)

