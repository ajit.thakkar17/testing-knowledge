﻿GIT, which stands for Global Information Tracker, is a powerful and widely-used version control system commonly used for software development and other collaborative projects.

GIT allows multiple developers/testers to work on a project simultaneously while ensuring that their changes do not interfere with one another.

It keeps track of all the changes made to the project and allows to revert to previous versions if necessary.

Below I have listed some basic commands, you can find the complete Git guide via this link. [Git - Reference (git-scm.com)](https://git-scm.com/docs)

1) **Git Init**

This command initialize a repository inside the GitDemo folder meaning that now on, it will track multiple versions of the files in the folder.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.001.png)

Initializing a Git repository will also create a folder called **.git** inside the repository folder. Files and folders with a period prefix (.) are typically private, and don’t show up by default.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.002.png)

When you list the files in a folder using **ls** command, you won't be able to see the .git folder but if you use the **ls -a** command, you can see the .git folder

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.003.png)

2) **Git status**

We should check the branch status quite frequently. It gives all the necessary information about the current status of the branch. We can check all the staged or untracked changes

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.004.png)

Now, let's add one file abc.txt and then check the status again

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.005.jpeg)

3) **Git add**

In git, files can be in one of the following three states: **modified**, **staged**, **committed**. If you are ready to commit files you have modified, you can add them to the staging area with the git add [file name] command.

Staged files are then marked for inclusion in the next commit without being committed yet:

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.006.png)

We can achieved the same result with git add .(or using a wildcard git add \*) . The only difference is that, in this way, all the files in the repository will be staged at the same time.

**Deciding to stage all files**

If the time is right to stage all files, there are several commands that you can choose from. As always, it's very important to know what you are staging and committing.

- **git add -A** : stages all files, including new, modified, and deleted files, including files in the current directory and in higher directories that still belong to the same git repository
    - **git add .** : adds the entire directory recursively, including files whose names begin with a dot

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.007.png) **git add -u** : stages modified and deleted files only, NOT new files

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.008.jpeg)

If we do the git status again, the file is now added to the staging area and ready for commit. It's now showing the file name in green

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.009.jpeg)

4) **Git config user.name/user.email**

Before your first commit, it’s good practice to tell git who you are. This is particularly important when you are working in a team so that each member can identify who made a certain commit

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.010.png)

If you wish to check your configuration settings, type **git config -l** or **git config - -list**

If you wish to check your user name and email, **git config user.name**  and **git config user. email**

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.011.png)

5) **Git remote add origin**

To connect this local repository to Github , we need to create a remote repository on Github. And connect the remote repository’s origin with the local repository.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.012.png)

6) **Git remote -v**

You can see the link of remote repository, same link for fetch and push

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.013.png)

7) **Git commit -m ‘ your message’**

A commit stores a snapshot of the files in the repository at a certain point in time. By building a history of these snapshots, you can rewind to an earlier point in time.

The **-m** flag indicates that you’re adding a message and the text in quotes that comes after it is the commit message itself.

It’s customary to make the commit message something informative, so if we do have to rewind or merge code, it’s obvious what changes have been made and when.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.014.png)

8) **Git push Origin master**

Finally, push the main branch on Github. **git push [remote name] [branch name]**

Once you have made changes to the local version of the repo, you can push them to the remote repo so that your project is safely stored in the cloud with its entire commits history.

This means that even if your laptop crashes or gets stolen, your work won’t be lost and could be retrieved from your GitHub account. To accomplish that, you need to **push the master branch to the remote *“origin”* repo**:

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.015.jpeg)

If you do   **Git push --set-upstream origin master**, then you can use only **Git push** next time as you have already set the upstream branch as Origin Master ( so no need to say **Git push Origin master** )

9) **Git log**

Using this command, we can see entire commit history.

Each commit is assigned with a 40 char long unique ID or hash. **Commit hashes are permanent** meaning that** Git preserves them and includes them in transfers between the local and remote repos.

Also notice how the name and email of the commit author are displayed under each commit history.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.016.jpeg)

10) **Git branch [branch name]**

This command will create the new branch but it will not switch user to newly created branch.

It is best practice create a new branch whenever you wish to make some changes to a project, and then merge that branch back into the master branch when we’re done.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.017.png)

11) **Git checkout -b [NEW-BRANCH-NAME]**

The checkout command creates the new branch. While the-b option switches or checks out from the current branch to the newly created branch.

It is always better to first switch to the main(master) branch before creating new branches.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.018.png)

Once the branch created, we can do all the works on that branch then perform

- Git Add .
    - Git Commit -m 'commit message'
        - Git push origin branchname  -  **this will create the new branch on github** and from there you can compare and create pull request and make it ready for review

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.019.png) Git merge [ new branch name]

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.020.png) Git push origin master (code will be merge with master branch and we won't see and option for creating pull request anymore)

12) **Git merge [new\_branch name]**

Merging allows us to copy commits from one branch into another.

This enables collaborative work as every team member can efficiently develop features for projects on their own branches without conflicts, then merge them into **master**.

To merge **branch1** into ***master,*** check out to to the master branch first and then run the git merge command

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.021.jpeg)

13) **Git Checkout [BRANCH-NAME]**

We can use Git checkout branch-name command to switch to the required branch

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.022.png)

14) **Git branch**

Git command to list all branches in the repository.

When you start working on a new project, it is always better to have a general idea about the branches in the repository.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.023.png)

15) **Git branch -r**

We can use git branch -r to show all of the branches on the remote and confirm that our branch is there.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.024.png)

16) **Git branch -a**

In contrast, git branch -a will show all of the branches available locally.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.025.png)

17) **Git branch -d [branch name]**

To delete the local branch - Once the commits have been merged into the ***master*** branch, local branch is often deleted.

18) **git push origin -d [branch name]** To delete the branch from remote
18) **Git reset**

    The git reset command is used to undo the changes in your working directory and get back to a specific commit while discarding all the commits made after that one.

    The  term  reset  stands  for  undoing  changes.  The  git  reset  command  is  used  to  reset  the changes. The git reset command has three core forms of invocation. These forms are as follows.

- **Soft**
    - **Mixed**
        - **Hard**

Each one is used depending on a specific situation: git reset --soft**,** git reset -- mixed**, and** git reset --hard

**Git Reset Hard**

It will first move the Head and update the index with the contents of the commits. It is the most direct, unsafe, and frequently used option. The --hard option changes the Commit History, and ref pointers are updated to the specified commit. Then, the Staging Index and Working Directory need to reset to match that of the specified commit. Any previously pending commits to the Staging Index and the Working Directory gets reset to match Commit Tree. It means any awaiting work will be lost.

For example, I have added file name HardReset.txt in working directory and perform git status and you can see it's showing as untracked file.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.026.png)

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.027.jpeg)

Now, I am adding this file into staging area using git add and perform the git status again

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.028.jpeg)

` `Now, I am going to perform the **reset --hard** option.

The -hard option is operated on the available repository. This option will reset the changes and match the position of the Head before the last changes. It will remove the available changes from the staging area.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.029.png)

If we perform git status now, we can see the output is displaying the status of the repository after the hard reset.

We can see there is nothing to commit in my repository because all the changes removed by the reset hard option to match the status of the current Head with the previous one.

So the file HardReset.txt has been removed from the repository.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.030.png)

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.031.png)

Generally, the reset hard mode performs below operations:

- It will move the HEAD pointer.
    - It will update the staging Area with the content that the HEAD is pointing.
        - It will update the working directory to match the Staging Area.

**Git Reset Mixed  OR  Git Reset**

A mixed option is a default option of the git reset command. If we would not pass any argument, then the git reset command considered as **--mixed** as default option.

A mixed option updates the ref pointers. The staging area also reset to the state of a specified commit. The undone changes transferred to the working directory.

From the below output, we can see that we have reset the position of the Head by performing the git reset -mixed command. Also, we have checked the status of the repository. As we can see that the status of the repository has not been changed by this command.

So it is clear that the mixed-mode does not clear any data from the staging area. Generally, the reset mixed mode performs the below operations:

- It will move the HEAD pointer
    - It will update the Staging Area with the content that the HEAD is pointing to.

It will not update the working directory as git hard mode does. It will only reset the index but not the working tree, then it generates the report of the files which have not been updated.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.032.jpeg)

**Git Reset Head (Git Reset Soft)**

The soft option does not touch the index file or working tree at all, but it resets the Head as all options do. When the soft mode runs, the refs pointers updated, and the resets stop there.

It will act as git amend command. It is not an authoritative command.

The --soft aims to change the HEAD (where the last commit is in your local machine) reference to a specific commit.

For instance, if we realize that we forgot to add a file to the commit, we can move back using the --soft with respect to the following format:

- git reset --soft HEAD~n to move back to the commit with a specific reference (n).
- git reset --soft HEAD~1 gets back to the last commit.
- git reset --soft commit ID moves back to the head with the commit ID.

For example, here I have added and committed two files, 1.txt and 2.txt. After that I have added another file 3.txt

Then I perform git reset --soft HEAD~1, which has removed my last commit, as you can see when doing git status its showing three files, ( 1.txt and 2.txt -  added but not committed,   and 3. txt needs to be add and commit )

Then i added 3.txt and preform commit which has committed all three files

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.033.jpeg)

**Git Reset to Commit**

Sometimes we need to reset a particular commit; Git allows us to do so. We can reset to a particular commit. To reset it, git reset command can be used with any option supported by reset command.

It will take the default behavior of a particular command and reset the given commit. The syntax for resetting commit is:
**git reset option commit-sha**

These options can be

- --soft
- --mixed ![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.034.png) --Hard
20) **Difference between Git merge and rebase**

The first thing to understand about git rebase is that it solves the same problem as git merge. Both of these commands are designed to integrate changes from one branch into another branch—they just do it in very different ways.

Consider what happens when you start working on a new feature in a dedicated branch, then another team member updates the main branch with new commits.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.035.png)

Now, let’s say that the new commits in main are relevant to the feature that you’re working on. To incorporate the new commits into your feature branch, you have two options: merging or rebasing.

**The Merge Option**

The easiest option is to merge the main branch into the feature branch using something like the following:

git checkout feature git merge main

This creates a new “merge commit” in the feature branch that ties together the histories of both branches, giving you a branch structure that looks like this:

Merging is nice because it’s a *non-destructive* operation. The existing branches are not changed in any way but this also means that the feature branch will have an extra merge commit every time you need to incorporate the main branch changes

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.036.png)

**The Rebase Option**

As an alternative to merging, you can rebase the feature branch onto main branch using the following commands:

git checkout feature git rebase main

This moves the entire feature branch to begin on the tip of the main branch, effectively incorporating all of the new commits in main. But, instead of using a merge commit, rebasing *re- writes* the project history by creating brand new commits for each commit in the original branch. The major benefit of rebasing is that you get a much cleaner project history. First, it eliminates the unnecessary merge commits required by git merge. Second, as you can see in the below diagram, rebasing also results in a perfectly linear project history

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.037.png)

21) **Git clean**

The git clean is an undo command. git clean command runs on untracked files. Untracked files are those created within the working directory, but are not yet added to the tracking index. The git clean command has various usages with different options.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.038.jpeg)

22) **Git rm**

In Git, the term rm stands for remove. It is used to remove individual files or a collection of files.

The key function of git rm is to remove tracked files from the Git index. Additionally, it can be used to remove files from both the working directory and staging index.

**Git Rm Cached**

Sometimes you want to remove files from the Git but keep the files in your local repository. In other words, you do not want to share your file on Git. Git allows you to do so. The cached option is used in this case. It specifies that the removal operation will only act on the staging index, not on the repository. The git rm command with cached option will be uses as:

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.039.jpeg)

**Git Rm -f**

This command will remove files from the Git and also remove it from your local repository.

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.040.jpeg)

23) **Git ignore**

When sharing your code with others, there are often files or parts of your project, you do not want to share.

Git can specify which files or parts of your project should be ignored by Git using a .gitignore file.

Git will not track files and folders specified in .gitignore. However, the .gitignore file itself is tracked by Git.

For example, in below gitognore file, we specify to ignore all the files in Private folder

![](Aspose.Words.80ace27d-ab19-4f57-b95d-75459d4b8743.041.png)
