Here are the 7 principles of software testing:

1. Testing Shows Presence of Defects: Testing is aimed at uncovering defects or issues in the software. Its primary objective is to identify problems and improve the quality of the software.
2. Exhaustive Testing is Impossible: It's practically impossible to test every possible input and scenario in a software system. Therefore, testing efforts should be focused on areas with higher risks and priorities.
3. Early Testing: Testing activities should start as early as possible in the software development lifecycle. Early testing helps in identifying defects sooner, which reduces the cost and effort of fixing them later in the process.
4. Defect Clustering: In software, defects tend to cluster around specific modules or components. This principle suggests that a small number of modules usually contain most of the defects.
5. Pesticide Paradox: If the same tests are repeated over and over, eventually they'll become ineffective in finding new defects. Test cases need to be regularly reviewed and updated to ensure they remain effective.
6. Testing is Context Dependent: Testing strategies, techniques, and priorities may vary based on the context of the project, including its requirements, technology, and constraints.
7. Absence-of-Errors Fallacy: The absence of detected defects in testing doesn't guarantee the absence of defects in the software. It's important to understand that testing can reduce but not completely eliminate the presence of defects.